import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./rootReducer";
import rootSaga from "./rootSaga";
import createSagaMiddleware from 'redux-saga';

const reduxSagaMonitorOptions = {};
const sagaMiddleware = createSagaMiddleware(reduxSagaMonitorOptions);
// sagaMiddleware.runSaga(rootSaga);

const middleware = [
    sagaMiddleware,
];

const store = configureStore({
    reducer: rootReducer,
    // enhancers: (defaultEnhancers) => [...enhancers, ...defaultEnhancers],
    middleware: (gDM) => gDM({
        serializableCheck: false
    }).concat([...middleware]),
    devTools: {
        shouldHotReload: false
    }
});

sagaMiddleware.run(rootSaga);

export default store;