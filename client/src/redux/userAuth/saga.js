import { put, call, delay, take, takeEvery, all } from "redux-saga/effects";
import { userAuthActions } from "./slice";
import { logInAPI } from "./remotes";

const { loginFailed, loginSuccess } = userAuthActions;

export function* loginSaga() {
    while (true) {
        // eslint-disable-line no-constant-condition
        console.log("entered saga");
        const action = yield take("userAuth/login");
        //   yield put(startSubmit(form));

        const { email, password, setLoadingAction } = action.payload;
        try {
            const result = yield call(logInAPI, email, password);
            yield put(loginSuccess({ user: result }));
        } catch (error) {
            const login_attemptsError = error.response && error.response.data && error.response.data || "Login Failed";
            yield put(loginFailed({ error: login_attemptsError }));
        } finally {
            // yield put(stopSubmit(form));
            if (setLoadingAction) {
                yield call(setLoadingAction);
            }
        }
    }
}

export function* userAuthSagas() {
    yield all([
        loginSaga()
    ])
}