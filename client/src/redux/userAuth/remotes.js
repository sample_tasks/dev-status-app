import api from "../../utils/api";

export function logInAPI(email, password) {
    return api
        .post(`/api/users/login`, { email, password })
        .then(response => response.data)
        .catch(error => Promise.reject(error));
}