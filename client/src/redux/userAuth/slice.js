import {
    createSlice,
    // type PayloadAction
} from '@reduxjs/toolkit';


const initialState = {
    user: {},
    error: false,loggedIn: false
};

export const userAuthSlice = createSlice({
    name: 'userAuth',
    initialState,
    reducers: {
        loginSuccess: (state, action) => {
            state.user = action.payload.user;
            state.loggedIn = true;
        },
        loginFailed: (state, action) => {
            state.error = action.payload.error;
            state.loggedIn = false;
        },
        clearError: (state, action) => {
            state.error = false;
        },
    },
});

export const { actions: userAuthActions, reducer: userAuthReducer } =
    userAuthSlice;
