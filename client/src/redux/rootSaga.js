import {
  all, fork, call
  // type AllEffect, type ForkEffect
} from 'redux-saga/effects';
import { userAuthSagas } from './userAuth/saga';

export default function* rootSaga() {
  yield all([
    fork(userAuthSagas),
    // fork(authonticationSagas)
  ]);
}
