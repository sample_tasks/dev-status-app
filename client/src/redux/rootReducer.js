

import { userAuthReducer } from "./userAuth/slice";
import { reducer as formReducer } from 'redux-form';

const rootReducer = {
  userAuth: userAuthReducer,
  form: formReducer
};

export default rootReducer;
