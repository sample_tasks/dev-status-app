import * as React from 'react';
import { LineChart } from '@mui/x-charts/LineChart';

const xLabels = [
    'CollectionDetail',
    "",
    'PipelineDetail',
    "",
    'ScheduleDepend',
  ];

 function SimpleLineChart() {
    return (<LineChart
  series={[
    { curve: "linear", data: [2,9,3,0,0] },
  ]}
  xAxis={[{ scaleType: 'point', data: xLabels }]}
//   width={280}
      height={350}
/>)
}

export default SimpleLineChart;