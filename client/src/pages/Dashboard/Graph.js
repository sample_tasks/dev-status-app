import * as React from 'react';
import { BarChart } from '@mui/x-charts/BarChart';

export default function BasicBars() {
    const uData = [3, 1, 1, 2];
const pData = [1, 0, 0, 0];
const xLabels = ['Connection', 'Configs', 'Pipeline', 'Schedule'];
  return (
    <BarChart
    series={[
        { data: uData, label: 'Active', id: 'uvId' },
        { data: pData, label: 'Inactive', id: 'pvId' },
      ]}
      xAxis={[{ data: xLabels, scaleType: 'band' }]}
      borderRadius={49}
    //   width={500}
      height={350}
    />
  );
}