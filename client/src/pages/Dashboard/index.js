import React from 'react';
import BasicBars from './Graph'; 
import SimpleLineChart from './LineChart';
import { Grid } from '@mui/material';
import PowerIcon from '@mui/icons-material/Power';
import StorageIcon from '@mui/icons-material/Storage';
import SwapCallsIcon from '@mui/icons-material/SwapCalls';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import CircularProgressWithLabel from './Progress';
import useStyles from './styles';
import { borderRadius, useTheme } from "@mui/system";

const Dashboard = () => {
    const theme = useTheme();
    const {classes} = useStyles();

    const obj = [
        {
            title: "connections",
            img: <PowerIcon className={classes.img}/>,
            total: 4,
            percentage: 75,
            background: "linear-gradient(to right, rgb(119 155 218), rgb(42 82 152 / 46%))"
        },
        {
            title: "configurations",
            img: <StorageIcon  className={classes.img}/>,
            total: 1,
            percentage: 100,
            background: "linear-gradient(to right, #ff7e5f, #feb47b)"
        },
        {
            title: 'pipelines',
            img: <SwapCallsIcon  className={classes.img}/>,
            total: 1,
            percentage: 100,
            background: "linear-gradient(to right, #56ab2f, #a8e063)"
        },
        {
            title: 'schedules',
            img: <AccessTimeIcon  className={classes.img}/>,
            total: 1,
            percentage: 100,
            background: "linear-gradient(to right, rgb(206 170 164), rgb(231 56 39 / 41%))"
        }
    ]

    return <div style={{display: 'flex',
        flexDirection: 'column',
        gap: theme.spacing(3)
    }}>
        <Grid container 
        style={{
            display: 'flex',
            alignItems: 'center',
            flexWrap: 'row',
        }}>
{(obj).map((ele,i) => (<Grid 
key={`${ele.title}-${i}`}
item
xs={12}
sm={3}
style={{
    height: '200px',
flex: 1,
display: 'flex',
flexDirection: 'column',
// justifyContent: 'center',
// alignItems: 'center',
background: ele?.background,
borderRadius: '15px',
padding: '30px'
}}>
    <div style={{display: 'flex',padding: '10px', justifyContent: 'space-between', alignItems: 'center'}}>
        <h3 style={{
            margin: "0px"
        }}>{ele?.title}</h3>
        {ele?.img}
    </div>
    <div style={{display: 'flex',padding: '10px 10px 0px 10px', justifyContent: 'space-between', alignItems: 'center'}}>
        <span>total counts: {ele?.total}</span>
        <CircularProgressWithLabel value={ele?.percentage} />
        </div>
</Grid>))}

        </Grid>
        <Grid container sx={{
            gap: theme.spacing(3)
        }}>
            <Grid sx={{
                border: '0.5px solid lightgray',
                flex: "1 1 0",
                flexBasis: '60%',
                maxWidth: 'calc(100% - 380px)',
                [`${theme.breakpoints.down('sm')}`]: {
                    flexBasis: '100%'
                },
                padding: '10px',
                borderRadius: '15px',
            }}>
            <SimpleLineChart />
            </Grid>
            <Grid sx={{
                border: '0.5px solid lightgray',
                flex: "1 1 0",
        maxWidth: '380px',
        [`${theme.breakpoints.down('sm')}`]: {
            flexBasis: '100%'
        },
                padding: '10px',
                borderRadius: '15px',
            }}>
            <BasicBars />
            </Grid>
        </Grid>
    </div>
}

export default Dashboard;