export const loginSchema = () => {
    return {
        columns: [
            {
                id: 1,
                name: 'email',
                label: 'Email',
                placeholder: 'eztekpay@gmail.com',
                editRecord: true,
                type: 'input',
                mandatory: true
            },
            {
                id: 2,
                name: 'password',
                label: 'Password',
                placeholder: '&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;&#8226;',//medium dots
                editRecord: true,
                type: 'input',
                mandatory: true
            },
        ]
    }
}