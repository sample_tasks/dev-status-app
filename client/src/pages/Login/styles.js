import { makeStyles } from "tss-react/mui";
const useStyles = makeStyles()((theme) => ({
    appContianer: {
        display: 'flex',
        // alignItems: 'center',
        width: '100%',
        height: '100vh',
        backgroundColor: "royalblue",
    },
    firstItem: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // padding: '10%',
        backgroundColor: "#FFF",
    },
    secondItem: {
        padding: '0px 5%',
        height: '100%',

    },
    cloudImg: {
        width: '80%'
    }
}));

export default useStyles;