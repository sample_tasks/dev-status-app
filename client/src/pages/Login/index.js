import React, { useEffect, useState } from "react";
import useStyles from "./styles";
import { loginSchema } from "./schema";
import InputField from "../../components/InputField";
import Form from "../../components/Form";
import { Link, useNavigate } from "react-router-dom";
import { Grid } from "@mui/material";
import LoginForm from "../../components/LoginForm";
import Cloud from "../../images/server1.svg";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";

const Login = () => {
    const { classes } = useStyles();
    // const [errors, setErrors] = useState({});
    // const [formData, setFormData] = useState({});
    const [btnload, setBtnLoad] = useState(false);
    const fields = (loginSchema().columns).filter(_ => _.editRecord);
    const user = useAppSelector(state => state?.userAuth?.user);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    // const validate = (values) => {
    //     const errors = {};
    //     const requiredFields = fields.filter(s => s.mandatory).map(e => e.name)
    //     if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    //         errors.email = 'Invalid email address';
    //     }

    //     requiredFields.forEach(field => {
    //         if (!values[field]) {
    //             errors[field] = 'Required'
    //         }
    //     })
    //     setErrors(errors);
    //     // return errors;
    // };

    // const handleChange = (values) => {
    //     console.log("values = ", values);
    //     const { name, value } = values || {};
    //     const obj = {
    //         ...formData,
    //         [name]: value
    //     }
    //     setFormData(obj);
    //     validate(obj);
    // }

    const handleSubmit = (props1) => {
        console.log("submit porps = ", props1);
        const { email, password } = props1;
        dispatch({ type: 'userAuth/login', payload: { email: email, password: password, setLoadingAction: () => setBtnLoad(false) } })
    }

    console.log("user = ", user);
    return <Grid container className={classes.appContianer}>
        <Grid item xs={12} sm={6} className={classes.firstItem}>
            {/* {(fields || []).map((field, i) => {
                return <InputField
                    key={`${field.id}_${i}`}
                    {...field}
                    onChange={handleChange} />
            })} */}
            <LoginForm
                fields={fields}
                form="login_form"
                onSubmit={handleSubmit}
                btnload={btnload}
                setBtnLoad={setBtnLoad}
            />
        </Grid>
        <Grid item xs={12} sm={6} className={classes.secondItem}>
            <h1>
                Migrate Date like a breeze
            </h1>
            <h3>
                High performance built in a minute
            </h3>
            <img src={Cloud} alt="cloud" className={classes.cloudImg} />
        </Grid>
    </Grid>
}

export default Login;