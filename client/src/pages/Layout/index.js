/**
 * Layout
 */

import React, { useEffect, useState } from "react";
import { useLocation, useNavigate, Outlet, Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
// import { logOut, sessionExpand, changePassword, sessionLogin, sessionTimeout, sessionClearTimeout } from "../../redux/app/actions";
// import store2 from 'store2';
import { Button, Grid, AppBar, Toolbar, IconButton, ListItemText, List, Typography, Hidden, Drawer, ListItemButton, ListItemIcon, Menu, MenuItem, useMediaQuery } from "@mui/material";
import Icons from "../../components/Icons";
import { useStyles } from './styles';
// import MenuIcon from '@mui/icons-material/Menu';
import { useTheme } from "@mui/system";
// import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
// import LeftIcon from '../../images/icons/Left.svg';
// import RightIcon from '../../images/icons/Right.svg';
import { appColor } from "../../utils/tools";
// import ModalRecordForm from "../../components/ModalRecordForm";
// import schema from "../../routes/schema";
// import { getFormValues } from "redux-form";
// import moment from "moment";
// import { visibility } from "../../utils/tools";
// import Spinner from "../../components/Spinner";

// const changePasswordColumns = schema().changePassword().columns;
// const loginFormColumns = schema().login().columns;
// const userSettingsColumns = schema().userMenuSettings().section;

function Layout(props) {
    const { pages } = props;
    let navigate = useNavigate();
    let location = useLocation();
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.down('md'));
    const sm = useMediaQuery(theme.breakpoints.down('sm'));

    const user = useAppSelector(state => state?.userAuth?.user)||{};

    // const user = useAppSelector((state) => state?.authentication?.user) || {};
    const loggedIn = useAppSelector((state) => state?.userAuth?.loggedIn) || false;
    // const expand = useAppSelector((state) => state?.authentication?.expand) || false;
    // const activeSession = useAppSelector((state) => state?.authentication?.activeSession) || false;
    // const metaData = useAppSelector((state) => state?.authentication?.metaData) || {};
    // const error = useAppSelector((state) => state?.authentication?.error) || false;
    // const hospital = useAppSelector((state) => state?.authentication?.hospital) || [];
    // const loading = useAppSelector((state) => state?.authentication?.loading) || false;
    // const appVersion = useAppSelector((state) => state?.authentication?.appVersion) || 0;

    // const formselector = useAppSelector(state => getFormValues(`settingsMenu_1`)(state));

    const dispatch = useAppDispatch();
    const { classes } = useStyles({ drawerWidth: sm ? 60 : 200 });
    // const [anchorEl, setAnchorEl] = React.useState(null);
    const [mobileOpen, setMobileOpen] = React.useState(true);
    // const [openModal, setOpenModal] = React.useState(true);
    // const [btnLoad, setBtnLoad] = React.useState(false);
    // // const [menuList, setMenuList] = React.useState(null);
    // // const [selectedMenu, setSelectedMenu] = React.useState(false);
    // const [formSection, setFormSection] = useState(0);

    // const field = userSettingsColumns[formSection];
    // const { title, message, confirmMessage, confirmButton, columns, schemaId, onSubmitClose } = field;
    // const errorMessage = error && (error.practice || error.changePassword || error.twoFactorAuthentication || error.sessionTimeout || error.signature);
    // const successMessage = success && (success.practice || success.changePassword || success.twoFactorAuthentication || success.signature);

    const pathData = location && location.pathname && pages && pages.length > 0 && pages.find(_ => _.data && _.data.path && location.pathname.includes(_.data.path)) || false;
    const activePath = location.pathname;
    // const { hospitalDetails, role, lastPasswordChangeDate = moment(), temporary_password, version } = user || {};
    // const { name, logo_file } = hospital[0] || {};

    // const open = Boolean(anchorEl);

    // const initialValues = false;

    // const formValues = (field.value === "changePassword") ? formselector : false;
    // const confirmationMessage = confirmMessage && typeof confirmMessage == "function" ? confirmMessage(user, formValues, metaData) : confirmMessage;

    useEffect(() => {
        if(user.name && activePath == "/"){
            navigate("/dashboard");
        } else if(!user.name && activePath != "/"){
            navigate("/");
        }
            },[user,location])


    const drawer = (
        <Grid
            sx={{
                height: '100%',
                // width: '100%',
                display: 'flex',
                flexDirection: 'column',
                // justifyContent: 'center',
                alignItems: 'center'
            }}>
            {/* {!mobileOpen ? <IconButton
                onClick={handleExpandToggle}
                className={expand ? classes.mobileMenuIcon : classes.menuIcon}>
                {(expand || mobileOpen) ? <img src={LeftIcon} width='30px' /> : <img src={RightIcon} width='30px' />}
            </IconButton> : null} */}
            {/* {(expand || mobileOpen) ? <Grid>
                <Typography variant="h5" className={classes.title}>
                    {name || ''}
                </Typography>
            </Grid> : null} */}
            {/* <Grid className={classes.toolbar}>
                {logo_file ? <img
                    src={logo_file || ''}
                    alt="Logo"
                    className={(expand || mobileOpen) ? classes.logo : classes.mobileLogo}
                    onClick={() => handleNavigate("/settings", location.state)}
                /> : null}

            </Grid> */}
            <List
                className={classes.customisedlist}
                sx={{
                    width: (sm) ?'36px'  : '174px',
                    margin: (sm) ? '0px 11px 0px 12px' : '0px 12px'
                }}
                component="nav" aria-label="main mailbox folders"
            >
                {(pages || []).map((page, index) => {
                    return (
                        <Grid key={index}
                            sx={{
                                width: '100%'
                            }}
                        >
                            <Link
                                key={index}
                                data-intercom-target={`tab-${page?.id}`}
                                to={page.data && page.data.path || '/'}
                                state={{ title: page.data && page.data.title }}
                                className={classes.link}
                                style={{
                                    width: (sm) ?'auto'  :"100%" ,
                                }}
                            >
                                {page.data.separator ? <div className={classes.separator} /> : null}

                                <ListItemButton
                                    key={index}
                                    size={"small"}
                                    sx={{
                                        m: 0,
                                        minWidth: (sm) ? 0 : "100%",
                                        display: 'flex',
                                        gap: '10px',
                                        width: 'auto',
                                        padding: (sm) ?'5px' : '7px 10px' ,
                                        // justifyContent: (expand || mobileOpen) ? 'flex-start' : 'center',
                                        borderRadius: "10px",
                                        border: `1px solid ${appColor}`,
                                        boxShadow: (activePath === page.data.path) || (page.data.path != '/' && activePath.indexOf(page.data.path) > -1) ? '0px 12px 23px 0px rgba(133, 133, 133, 0.25)' : "none"
                                    }}
                                    // onClick={() => setMobileOpen(false)}
                                    selected={(activePath === page.data.path) || (page.data.path != '/' && activePath.indexOf(page.data.path) > -1)}
                                >
                                    <Icons
                                        type={(page.data && page.data.icon) || ''}
                                        color={(activePath === page.data.path) || (page.data.path != '/' && activePath.indexOf(page.data.path) > -1) ? "#163266" : '#fff'}
                                        style={page?.data?.iconstyle || {}}
                                    />
                                    {(!sm) && <Typography variant="bodySpan1" primary={(page.data && page.data.title) || ''} sx={{ textTransform: 'capitalize', color: (activePath === page.data.path) || (page.data.path != '/' && activePath.indexOf(page.data.path) > -1) ? "#163266" : '#fff' }}>
                                        {(page.data && page.data.title) || ''}
                                    </Typography> || null}
                                </ListItemButton>

                            </Link>
                        </Grid>
                    )
                }
                )
                }
            </List>
        </Grid>
    );

    return (
        <Grid name="layoutcontainer" container sx={{
            height: '100%',
            flexWrap: 'nowrap'
        }}>
            {(loggedIn && user?.name) ? <Grid>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar
                        className={classes.toolbar}
                        sx={
                            !md ? {
                                justifyContent: 'space-between'
                            } : {
                                '& :last-child': {
                                    marginLeft: 'auto'
                                }
                            }
                        }
                        variant='regular'
                    >
                         {/* <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            edge="start"
                            onClick={handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton> */}
                        <Typography
                            variant="bodySpan2"
                            className={classes.appBarTitle}
                        >
                            {pathData && pathData.data && pathData.data.title || ''}
                        </Typography>
                        
                    </Toolbar>
                </AppBar>
                <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open>
                                {drawer}
                                </Drawer>
                </Grid> : null}
                {<main className={loggedIn ? classes.content : classes.login} style={{
                    // minHeight: containerHeight
                }}>
                    <Outlet />
                </main>}
        </Grid>
    );
}

export default Layout;