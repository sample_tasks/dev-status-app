import { IconButton } from "@mui/material";
import React from "react";
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import EditIcon from '@mui/icons-material/Edit';

const ActionComponent = () => {
    return <div style={{
        display: 'flex',
        flexFlow: 'row'
    }}>
        <IconButton>
<EditIcon />
    </IconButton>
    <IconButton>
        <VisibilityIcon />
        </IconButton>
        </div>
}

export default ActionComponent;