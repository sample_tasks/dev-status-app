import { useEffect, useState } from 'react';
import './styles.css';
import { useTheme } from "@mui/system";
import { Button, Switch } from '@mui/material';
import moment from "moment";
import ActionComponent from './ActionComponent';

function RecordsPage() {
  const [products, setProducts] = useState([])
  const [page, setPage] = useState(1);
  const theme = useTheme();

  const fetchProducts = async () => {
    const res = await fetch(`http://localhost:5000/api/record`)
    const data = await res.json()

    console.log("data = ",data);

    if (data) {
      setProducts(data||[])
    }
  }

  useEffect(() => {
    fetchProducts()
  }, [])

  const selectPageHandler = (selectedPage) => {
    console.log("oafete = ",selectedPage);
    if (selectedPage !== page) {
      setPage(selectedPage)
    }
  }
console.log("page = ",page,"cond = ",)
  return (
    <div className='recordsPageContainer' style={{
        height: `calc(95vh - (48px + 2 * ${theme.spacing(3)}))`,
        [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
            height: `calc(95vh - (38px + 2 * ${theme.spacing(3)}))`
        },
        [theme.breakpoints.up('sm')]: {
            height: `calc(95vh - (56px + 2 * ${theme.spacing(3)}))`
        },
    }}>
        <table className='table'>
            <thead>
                <tr>
                    <th className='thead'>
                        S.No
                    </th>
                    <th className='thead'>
                        Connection Name
                    </th>
                    <th className='thead'>
                        Description
                    </th>
                    <th className='thead'>
                        Start Date
                    </th>
                    <th className='thead'>
                        End Date
                    </th>
                    <th className='thead'>
                        Active
                    </th>
                    <th className='thead'>
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
            {products.length > 0 &&
        products.slice(page * 5 - 5, page * 5).map((ele, i) => {
          return <tr className='tdatarow' key={`${i}-${ele?.connectionName}`}>
                <td className='tdata'>
                        {ele?.connectionName}
                    </td>
                    <td className='tdata'>
                        {ele?.connectionName}
                    </td>
                    <td className='tdata'>
                        {ele?.description}
                    </td>
                    <td className='tdata'>
                        {/* {ele?.start_date} */}
                        {moment(ele?.start_date).format("MM-DD-YYYY")}
                    </td>
                    <td className='tdata'>
                        {moment(ele?.end_date).format("MM-DD-YYYY")}
                    </td>
                    <td className='tdata'>
                        {/* {ele?.active} */}
                        <Switch
  checked={ele?.active.toLowerCase() == "true"}
/>
                    </td>
                    <td className='tdata'>
                        {/* {ele?.connectionName} */}
                        <ActionComponent />
                    </td>
                </tr>
                 })}
            </tbody>
        </table>
      {/* {products.length > 0 && <div className="products">
        {products.slice(page * 5 - 5, page * 5).map((prod) => {
          return <span className="products__single" key={prod.id}>
            <img src={prod.thumbnail} alt={prod.title} />
            <span>
              {prod.title}
            </span>
          </span>
        })}
      </div>} */}

      {products.length > 0 && <div className="pagination">
        <Button 
            onClick={() => selectPageHandler(page - 1)} 
            className={page > 1 ? "pagibationbtn" : "pagination__disable"}
            disabled={page > 1}
            ><span>◀</span></Button>

        {[...Array(Math.ceil(products.length / 5))].map((_, i) => {
          return <Button key={i} className={page === i + 1 ? "pagination__selected" : "pagibationbtn"} onClick={() => selectPageHandler(i + 1)}><span>{i + 1}</span>
          </Button>
        })}

<Button 
onClick={() => selectPageHandler(page + 1)} 
className={page < products.length / 5 ? "pagibationbtn" : "pagination__disable"}
disabled={page < products.length / 5}
><span>▶</span></Button>
      </div>}
    </div>
  );
}

export default RecordsPage;