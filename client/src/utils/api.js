import axios from 'axios';
// import constants from '../common/apiConstants';

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL || 'http://localhost:5000/',
    timeout: 40000,
    headers: { Accept: 'application/json' },
});

export function setAuthToken(authToken) {
    api.defaults.headers.common['Authorization'] = authToken;
}

export default api;
