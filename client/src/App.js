// import logo from './logo.svg';
import React, { lazy, Suspense } from 'react';
import './App.css';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from './pages/Layout';

const Login = lazy(() => import('./pages/Login'));
const Dashboard = lazy(() => import('./pages/Dashboard'));
const RecordsPage = lazy(() => import('./pages/RecordsPage'));


const routesProvider = [{
  name: 'dashboard',
  data:{
    path:'/dashboard',
    title: "Dashboard",
    icon: 'Dashboard'
  }
},
{
  name: 'records',
  data:{
    path:'/records',
    title: "Records",
    icon: 'GroupIcon'
  }
}]

function App() {
  return (
    // <div className="App">
    <BrowserRouter>
      <Routes>
      <Route element={<React.Suspense fallback={"loading..."}>
                <Layout pages={routesProvider.filter(_ => _.data && !_.data.route)} />
            </React.Suspense>}
            >
        <Route
          path="/"
          element={<Suspense fallback="...loading"><Login /></Suspense>} />
        <Route
          path="/dashboard"
          element={<Suspense fallback="...loading"><Dashboard /></Suspense>} />
        <Route
          path="/records"
          element={<Suspense fallback="...loading"><RecordsPage /></Suspense>} />
          </Route>
      </Routes>
    </BrowserRouter>
    // </div>
  );
}

export default App;
