
import InputField from "../InputField";
import CustomInput from "../CustomInput";
import PasswordField from "../PasswordField";

export const ImplementationFor = {
    input: CustomInput,
    password: PasswordField
};
