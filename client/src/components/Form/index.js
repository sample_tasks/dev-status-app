/**
 * 
 * Login Form
 * 
 */


import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Typography, useMediaQuery } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import useStyles from './styles';
// import ErrorMessage from '../Error';
import { ImplementationFor } from './utils';
// import EzpayTekLogo from '../../images/EzTekPAY.svg';
import { useTheme } from "@mui/system";
import { Field, reduxForm } from 'redux-form';
import validate from '../../utils/validation';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function Form(props) {
    const {
        handleSubmit,
        locationState = {},
        errorMessage,
        clearCache,
        btnload,
        setBtnLoad,
        fields,
        // initial,
        submitting,
        destroy
    } = props;

    const { classes } = useStyles();
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.down('sm'));
    const lg = useMediaQuery(theme.breakpoints.down('lg'));
    const xl = useMediaQuery(theme.breakpoints.up('xl'));

    useEffect(() => {
        return () => destroy();
    }, []);

    return (
        <form onSubmit={handleSubmit} className={classes.form} noValidate>
            {(fields || []).map((field, index) => {
                const InputComponent = ImplementationFor[field.type];
                return <div key={index} sx={{
                }}>
                    <Field
                        key={index}
                        name={field.value}
                        label={field.label}
                        component={InputComponent}
                        placeholder={field.placeholder}
                        options={field.options}
                    />
                </div>
            })}
            <div sx={{
                paddingTop: '12px'
            }}>
                <LoadingButton
                    className={classes.loadingButton}
                    loading={btnload}
                    type='submit'
                    fullWidth
                    variant='contained'
                    color='primary'
                >Log In
                </LoadingButton>
            </div>

            {/* {errorMessage &&
                    <Grid sx={{
                        textAlign: 'center'
                    }}
                        xs={12}>
                        <ErrorMessage errorMessage={errorMessage} onClose={clearCache} />
                    </Grid> || null} */}

            {/* <Grid sx={{
                textAlign: 'center',
                display: 'block'
            }} xs={12}>
                <Typography variant='subTitle2' sx={{
                    fontSize: '15px',

                }}>
                    Don&apos;t have an account?&nbsp;
                    <Link to="/" state={{ form: 'signup1' }}
                        className={classes.linkColor}
                    >
                        Sign up
                    </Link>
                </Typography>
            </Grid> */}
        </form>
    )
}

export default reduxForm({
    form: 'reponsive_Form',
    enableReinitialize: true,
    validate,
    touchOnChange: true,
})(Form);