// import express from 'express';
// import Record from '../../models/Record';
const express = require("express");
const Record = require('../../models/Record');

const router = express.Router();

// This section will help you get a list of all the records.
router.get('/', async (req, res) => {
  try {
    const records = await Record.find();
    res.status(200).json(records);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server error' });
  }
});

// This section will help you get a single record by id
router.get('/:id', async (req, res) => {
  try {
    const record = await Record.findById(req.params.id);
    if (!record) return res.status(404).json({ message: 'Record not found' });
    res.status(200).json(record);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server error' });
  }
});

// This section will help you create a new record.
router.post('/', async (req, res) => {
  try {
    const newRecord = new Record({
      name: req.body.name,
      position: req.body.position,
      level: req.body.level,
    });
    const record = await newRecord.save();
    res.status(201).json(record);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error adding record' });
  }
});

// This section will help you update a record by id.
router.patch('/:id', async (req, res) => {
  try {
    const updates = {
      name: req.body.name,
      position: req.body.position,
      level: req.body.level,
    };
    const record = await Record.findByIdAndUpdate(req.params.id, updates, { new: true });
    if (!record) return res.status(404).json({ message: 'Record not found' });
    res.status(200).json(record);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error updating record' });
  }
});

// This section will help you delete a record
router.delete('/:id', async (req, res) => {
  try {
    const record = await Record.findByIdAndDelete(req.params.id);
    if (!record) return res.status(404).json({ message: 'Record not found' });
    res.status(200).json({ message: 'Record deleted' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error deleting record' });
  }
});

module.exports = router;

// export const sampleCode = {};