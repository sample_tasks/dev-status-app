// import { MongoClient, ServerApiVersion } from "mongodb";

// const uri = process.env.ATLAS_URI || "";
// // const uri = 'mongodb://0.0.0.0:27017';

// const client = new MongoClient(uri, {
//     serverApi: {
//         version: ServerApiVersion.v1,
//         strict: true,
//         deprecationErrors: true,
//     },
// });

// try {
//     console.log('Attempting to connect to MongoDB...');
//     // Connect the client to the server
//     await client.connect();
//     // Send a ping to confirm a successful connection
//     await client.db("admin").command({ ping: 1 });
//     console.log(
//         "Pinged your deployment. You successfully connected to MongoDB!"
//     );
// } catch (err) {
//     console.error(err);
// }

// let db = client.db("employees");

// export default db;

// ----------------------------------------------------------------------

// import mongoose from 'mongoose';
// import dotenv from 'dotenv';
const mongoose = require("mongoose");
const dotenv = require("dotenv");


dotenv.config();

const uri = process.env.ATLAS_URI;

const connectDB = async () => {
    try {
        console.log('Attempting to connect to MongoDB...');
        await mongoose.connect(uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });
        console.log('MongoDB Connected');
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
};

export default connectDB;
