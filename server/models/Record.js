const mongoose = require("mongoose");

const recordSchema = new mongoose.Schema({
    connectionName: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    active: {
        type: String,
        required: true,
    },
    start_date: {
        type: Date,
        default: Date.now,
    },
    end_date: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model('connections', recordSchema);
